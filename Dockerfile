FROM eclipse-temurin:17-jdk

WORKDIR /app

COPY target/order_service.jar order_service.jar

COPY config ./config

EXPOSE 8082

CMD ["java", "-jar", "order_service.jar", "--spring.config.additional-location=file:./config/"]