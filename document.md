## **Order Service Application Deployment Guide Service**

### **Introduction**

This guide will walk you through the process of deploying the Order Service application using the Spring Boot framework v2. This application is an order management service providing APIs for data access
### **Technology stack on the server side**
- Programming Language: Java, version 17.
- Spring Boot: Framework is used as the main platform for building the application.
- Maven: Configuration for building, testing and running the application
- Spring Security:  Used for protecting and managing security within the application.
- Spring Data JPA: Used for interacting with the database using Java Persistence API (JPA).
- Spring MVC REST: Provides support for developing web applications with Spring MVC and embedded Tomcat server.
- Spring Validation: Provides support for data validation and verification.
- JUnitTest + Mockito: Used for writing tests for the application.
- MySQL DB
- H2 Database: Embedded database used in the test environment.
- OpenAPI UI: Used to generate and display API documentation in OpenAPI (Swagger) format.
- Actuator: Provides management and monitoring tools for Spring Boot applications.
- Docker(optional)


### **Requirements**

Before deployment, make sure you have installed :

- Java Development Kit (JDK) 17 or higher
- MySQL Server
- Docker management tool (optional)

### **Step 1: Database Configuration**

- Ensure that MySQL Server is installed and running.
- Create a new database for the application: order_service.
- Create a MySQL user account with access to that database.
- Edit connection details in the application.yml file in the config folder:
```yaml
spring:
  datasource:
    url: jdbc:mysql://${IP_SERVER}:3306/order_service?useSSL=false&allowPublicKeyRetrieval=true
    username: ${username_sql}
    password: ${password_sql}
```

### **Step 2: Security Configuration**

Edit basic authentication in the application.yml file if necessary:
```yaml
spring:
  security:
  user:
    name: xxx
    password: xxx
```

### **Step 3: Logging Configuration**

Edit the log file path in application.yml if necessary:
```yaml
logging:
  config: config/logback.xml
```

### **Step 4: Port and Context Path Configuration**

Edit the port and context path in application.yml:

```yaml
server:
  port: 8082
  servlet:
    context-path: /esoft_service
```

### **Step 5: Application Deployment**

- Open Terminal/CMD, navigate to the project's root directory.
- Run the following command to package the application into a JAR:
```cmd
mvn clean package
```
After the packaging process is complete, the JAR file will be located at .
```cmd
target/order_service.jar
```
Run the application using the following command:
```cmd
java -jar target/order_service.jar
```

### **Step 6: Deployment with Docker (Optional)**

- Create a Dockerfile in the project's root directory with the following content:

```Dockerfile
FROM eclipse-temurin:17-jdk

WORKDIR /app

COPY target/order_service.jar order_service.jar

COPY config ./config

EXPOSE 8082

CMD ["java", "-jar", "order_service.jar", "--spring.config.additional-location=file:./config/"]
```

- Create a docker-compose file in the project's root directory with the following content:

```yml
version: '3'

services:
  order-app:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8082:8082"
    depends_on:
      - db
    environment:
      SPRING_DATASOURCE_URL: jdbc:mysql://db:3306/order_service
      SPRING_DATASOURCE_USERNAME: orderuser
      SPRING_DATASOURCE_PASSWORD: orderpassword
    volumes:
      - ./config:/app/config

  db:
    image: mysql:8.0
    environment:
      MYSQL_ROOT_PASSWORD: orderpassword
      MYSQL_DATABASE: order_service
    volumes:
      - db-data:/var/lib/mysql
      - ./init-db:/docker-entrypoint-initdb.d
    command: --init-file /docker-entrypoint-initdb.d/init.sql

volumes:
  db-data:
```
Then run the following command to start the application
```cmd
docker-compose up --build -d
```

### **Step 7: Deployment Verification**

- The Spring Boot application will start at [**http://localhost:808**](http://localhost:8080/)**2.**
- Access this address from a web browser to verify if the application has been successfully deployed
- Monitor the system through healthcheck and metrics endpoints:
    - [**http://localhost:8082/esoft_service/actuator/health**](http://localhost:8082/esoft_service/actuator/health)
    - [**http://localhost:8082/esoft_service/actuator/metrics**](http://localhost:8082/esoft_service/actuator/metrics)

### **Step 8: Access API Documentation**

Open a browser and access the following address to view the API documentation:

- [**http://localhost:8082/esoft_service/swagger-ui.html**](http://localhost:8082/esoft_service/actuator/metrics)

### **Step 9: Authentication**

Use the configured login information to access the application's features.