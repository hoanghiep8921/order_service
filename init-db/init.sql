CREATE DATABASE IF NOT EXISTS order_service;
CREATE SCHEMA IF NOT EXISTS `order_service` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;

CREATE USER IF NOT EXISTS 'orderuser'@'%' IDENTIFIED BY 'orderpassword';
GRANT ALL PRIVILEGES ON order_service.* TO 'orderuser'@'%';
FLUSH PRIVILEGES;

