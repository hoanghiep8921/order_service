package com.esoft.order_service.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);
    private static final String REQUEST_ID = "REQUEST_ID";
    private final ObjectMapper objectMapper;

    @Autowired
    public LoggingAspect(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Before("execution(* com.esoft.order_service.controllers..*.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getTarget().getClass().getName();
        HttpServletRequest request = getRequest();
        MDC.put(REQUEST_ID, request.getHeader("x-request-id"));
        LOGGER.debug("Before method execution: {}.{} with RequestBody: {}", className, methodName, joinPoint.getArgs()[0]);
    }

    @AfterReturning(pointcut = "execution(* com.esoft.order_service.controllers..*.*(..))", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getTarget().getClass().getName();
        try {
            HttpServletRequest request = getRequest();
            MDC.put(REQUEST_ID, request.getHeader("x-request-id"));
            String responseBody = objectMapper.writeValueAsString(result);
            LOGGER.debug("After method execution: {}.{} - Response Body: {}", className, methodName, responseBody);
            MDC.remove(REQUEST_ID);
        } catch (Exception e) {
            LOGGER.error("Error converting response body to JSON", e);
        }
    }

    @AfterThrowing(pointcut = "execution(* com.esoft.order_service.controllers..*.*(..))", throwing = "exception")
    public void handleException(JoinPoint joinPoint, Throwable exception) {
        LOGGER.error("Exception during method execution: {}", exception.getMessage());
        MDC.remove(REQUEST_ID);
    }

    @Around("execution(* com.esoft.order_service.service.*.*(..))")
    public Object monitorPerformance(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        LOGGER.debug(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return result;
    }
    @After(value = "execution(* com.esoft.order_service.service.*.*(..))")
    public void after(JoinPoint joinPoint) {
        LOGGER.info("after execution of {}", joinPoint);
    }
    private HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }
}
