package com.esoft.order_service.constant;

public enum CustomerType {
    CUSTOMER, ADMINISTRATOR
}
