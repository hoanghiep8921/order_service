package com.esoft.order_service.constant;

public enum MessageResponse {
    ERROR("Lỗi", "ERROR"),
    SUCCESS("Thành công", "SUCCESS");

    private String valueVi;
    private String valueEn;

    MessageResponse(String valueVi, String valueEn) {
        this.valueVi = valueVi;
        this.valueEn = valueEn;
    }

    public String getValue(String language) {
        if ("en".equalsIgnoreCase(language)) {
            return valueEn;
        } else {
            return valueVi;
        }
    }
}
