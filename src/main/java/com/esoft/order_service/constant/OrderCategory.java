package com.esoft.order_service.constant;

public enum OrderCategory {
    LUXURY, SUPER_LUXURY, SUPREME_LUXURY
}
