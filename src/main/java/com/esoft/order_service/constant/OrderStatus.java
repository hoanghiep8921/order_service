package com.esoft.order_service.constant;

public enum OrderStatus {
    PENDING, CONFIRMED, CANCELLED
}
