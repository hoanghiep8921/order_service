package com.esoft.order_service.constant;

public enum ServiceCode {
    SUCCESS("00"), ERROR("99"), NOT_FOUND("04");
    private final String code;

    ServiceCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return code;
    }
}
