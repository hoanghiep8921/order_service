package com.esoft.order_service.constant;

public enum ServiceName {
    PHOTO_EDITING, VIDEO_EDITING
}
