package com.esoft.order_service.controllers;

import com.esoft.order_service.dto.core.*;
import com.esoft.order_service.util.Utils;
import com.esoft.order_service.dto.response.BaseResponse;
import com.esoft.order_service.dto.request.CreateOrderRequest;
import com.esoft.order_service.dto.request.UpdateOrderRequest;
import com.esoft.order_service.exceptions.NotFoundException;
import com.esoft.order_service.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/orders")
@Slf4j
@Tag(name = "Order Management")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get list Order by filter",
               description = "Retrieves list Order based on the filter",
               responses = {
                       @ApiResponse(responseCode = "200", description = "Order found"),
                       @ApiResponse(responseCode = "404", description = "Order not found"),
                       @ApiResponse(responseCode = "500", description = "Internal server error")
               })
    public ResponseEntity<BaseResponse<Page<OrderDTO>>> getListOrderOfCustomer(@Parameter(name = "language", description = "Language of the message response", example = "en", required = false)
                                                                               @RequestHeader(name = "language", defaultValue = "vi") String lang,
                                                                               @Parameter(name = "customerId", description = "Id of the customer", example = "1", required = true)
                                                                               @RequestParam("customerId") Long customerId,
                                                                               @Parameter(name = "page", description = "Page contain content", example = "0", required = true)
                                                                               @RequestParam(defaultValue = "0") int page,
                                                                               @Parameter(name = "size", description = "Size of page request", example = "10", required = true)
                                                                               @RequestParam(defaultValue = "10") int size) throws NotFoundException {
        Pageable pageable = PageRequest.of(page, size);
        return Utils.createSuccessResponse(orderService.getUserOrders(customerId, pageable), lang,false);
    }

    @Operation(summary = "Create an order",
            description = "Create a new order with the necessary information by customer",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order found"),
                    @ApiResponse(responseCode = "404", description = "Order not found"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<BaseResponse<OrderDTO>> createOrder(@Parameter(name = "language", description = "Language of the message response", example = "en", required = false)
                                                              @RequestHeader(name = "language", defaultValue = "vi") String lang,
                                                              @Valid @RequestBody CreateOrderRequest createOrderRequest) throws NotFoundException {
        return Utils.createSuccessResponse(orderService.saveOrder(createOrderRequest), lang,true);
    }

    @GetMapping("/{orderId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a Order by ID",
               description = "Retrieves a Order based on the provided ID",
               responses = {
                       @ApiResponse(responseCode = "200", description = "Order found"),
                       @ApiResponse(responseCode = "404", description = "Order not found"),
                       @ApiResponse(responseCode = "500", description = "Internal server error")
               })
    public ResponseEntity<BaseResponse<OrderDTO>> findOrder(@Parameter(name = "language", description = "Language of the message response", example = "en", required = false)
                                                            @RequestHeader(name = "language", defaultValue = "vi") String lang,
                                                            @PathVariable("orderId") Long orderId) throws NotFoundException {
        return Utils.createSuccessResponse(orderService.findById(orderId), lang,false);
    }

    @Operation(summary = "Update an order by id",
            description = "Update some specific fields in a specific order with the order id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order found"),
                    @ApiResponse(responseCode = "404", description = "Order not found"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @PutMapping("/{orderId}/update")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BaseResponse<OrderDTO>> updateOrder(@Parameter(name = "language", description = "Language of the message response", example = "en", required = false)
                                                              @RequestHeader(name = "language", defaultValue = "vi") String lang,
                                                              @Parameter(name = "orderId", description = "ID of the order", example = "1", required = true)
                                                              @PathVariable("orderId") Long orderId,
                                                              @RequestBody UpdateOrderRequest updateOrderRequest) throws NotFoundException {
        return Utils.createSuccessResponse(orderService.updateOrder(orderId, updateOrderRequest), lang,false);
    }

    @Operation(summary = "Delete an order by id",
            description = "Delete a specific order using the order id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order found"),
                    @ApiResponse(responseCode = "404", description = "Order not found"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @DeleteMapping("/{orderId}/delete")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BaseResponse<Object>> deleteOrder(@Parameter(name = "language", description = "Language of the message response", example = "en", required = false)
                                                            @RequestHeader(name = "language", defaultValue = "vi") String lang,
                                                            @Parameter(name = "orderId", description = "ID of the order", example = "1", required = true)
                                                            @PathVariable("orderId")
                                                            Long orderId) {
        orderService.deleteById(orderId);
        return Utils.createSuccessResponse(lang,false);
    }
}
