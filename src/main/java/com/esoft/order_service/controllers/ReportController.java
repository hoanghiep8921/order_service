package com.esoft.order_service.controllers;

import com.esoft.order_service.dto.core.*;
import com.esoft.order_service.dto.response.BaseResponse;
import com.esoft.order_service.service.OrderService;
import com.esoft.order_service.util.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/reports")
@Slf4j
@Tag(name = "Report Order")
public class ReportController {
    private final OrderService orderService;
    @Autowired
    public ReportController(OrderService orderService) {
        this.orderService = orderService;
    }

    //If the number of records is large,
    //we should deploy data collection according to time period and Pageable
    @Operation(summary = "Summary number of orders placed by a user",
            description = "Summary number of orders placed by a user",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Summary success"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @GetMapping("/customer-order-summaries")
    public ResponseEntity<BaseResponse<List<CustomerOrderSummaryDTO>>> getCustomerOrderSummaries(HttpServletRequest request) {
        return Utils.createSuccessResponse(orderService.countOrdersByCustomerId());
    }
    @Operation(summary = "Summary revenue from a user",
            description = "Summary revenue from a user",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Summary success"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @GetMapping("/customer-revenue-summaries")
    public ResponseEntity<BaseResponse<List<CustomerRevenueSummaryDTO>>> getCustomerRevenueSummaries(HttpServletRequest request) {
        return Utils.createSuccessResponse(orderService.calculateRevenueByCustomerId());
    }

    @Operation(summary = "Summary number of orders and revenue",
            description = "Summary number of orders and revenue placed within a specific of time (a month, a year)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Summary success"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @GetMapping("/order-summaries")
    public ResponseEntity<BaseResponse<OrderSummaryDTO>> getOrderSummaries(@Parameter(name = "startDate", description = "Query start time", example = "2024-01-01T00:00:00", required = true)
                                                    @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
                                                    @Parameter(name = "endDate", description = "Query end time", example = "2024-03-01T00:00:00", required = true)
                                                    @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate
    ) {
        return Utils.createSuccessResponse(orderService.getOrderSummary(startDate,endDate));
    }


    @Operation(summary = "Summary number of orders and revenue",
            description = "Summarize the number of orders and revenue by each type of service ordered in a specific period of time (a month, a year)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Summary success"),
                    @ApiResponse(responseCode = "500", description = "Internal server error")
            })
    @GetMapping("/service-type-order-summaries")
    public ResponseEntity<BaseResponse<List<ServiceTypeOrderSummaryDTO>>> getServiceTypeOrderSummaries(
            @Parameter(name = "startDate", description = "Query start time", example = "2024-01-01T00:00:00", required = true)
            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @Parameter(name = "endDate", description = "Query end time", example = "2024-03-01T00:00:00", required = true)
            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate
    ) {
        return Utils.createSuccessResponse(orderService.getOrderSummaryByServiceType(startDate,endDate));
    }
}
