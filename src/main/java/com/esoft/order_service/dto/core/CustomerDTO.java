package com.esoft.order_service.dto.core;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Represents an customer in the system",
        example = """
        {
            "id": 123,
            "name": "hiepdh",
            "customerType": "ADMINISTRATOR"
        }
        """)
public class CustomerDTO {
    @Schema(description = "The unique identifier of the customer")
    private Long id;
    @Schema(description = "One of these values: CUSTOMER, ADMINISTRATOR")
    private String customerType;
    @Schema(description = "Name of customer")
    private String name;
}
