package com.esoft.order_service.dto.core;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Represents reporting on summary number of orders placed by a user",
        example = """
        {
            "customerId": 123,
            "name": "hiepdh",
            "orderCount": "20",
        }
        """)
@Builder
public class CustomerOrderSummaryDTO {
    private Long customerId;
    private String name;
    private Long orderCount;
}
