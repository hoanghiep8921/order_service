package com.esoft.order_service.dto.core;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Represents reporting on summary revenue from a user",
        example = """
        {
            "customerId": 123,
            "name": "hiepdh",
            "orderCount": "20",
        }
        """)
public class CustomerRevenueSummaryDTO {
    private Long customerId;
    private String name;
    private BigDecimal totalRevenue;
}
