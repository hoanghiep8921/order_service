package com.esoft.order_service.dto.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "Represents an order in the system",
        example = """
        {
          "id": 123456,
          "orderReference": "ORD123456",
          "serviceName": "Service A",
          "orderCategory": "Category X",
          "status": "PENDING",
          "customer": {
            "id": 789,
            "name": "hiepdh",
            "customerType": "CUSTOMER"
          },
          "quantity": 2,
          "description": "Description of the order",
          "totalPrice": 99.99,
          "notes": "Additional notes about the order",
          "address": "456 Elm St, Othertown, CA 54321"
        }
        """)
public class OrderDTO {
    @Schema(description = "The unique identifier of the order")
    private Long id;
    @Schema(description = "A unique string which provide short description about the order")
    private String orderReference;
    @Schema(description = "One of these values: PHOTO_EDITING, VIDEO_EDITING")
    private String serviceName;
    @Schema(description = "One of these values: LUXURY, SUPER_LUXURY, SUPREME_LUXURY")
    private String orderCategory;
    @Schema(description = "Status of the order")
    private String status;
    @Schema(description = "Information about the customer placing the order")
    private CustomerDTO customer;
    @Schema(description = "Number of items to be served in this order")
    private Integer quantity;
    @Schema(description = "Description of the order")
    private String description;
    @Schema(description = "total amount of the order")
    private BigDecimal totalPrice;
    @Schema(description = "note for order")
    private String notes;
    @Schema(description = "Delivery address")
    private String address;
    @Schema(description = "Order creation date")
    private LocalDateTime orderCreatedAt;
}
