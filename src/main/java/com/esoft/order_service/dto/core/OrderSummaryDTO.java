package com.esoft.order_service.dto.core;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Represents reporting on summary number of orders and revenue placed via this service within a specific of time (a month, a year)",
        example = """
        {
            "totalOrders": 123,
            "totalRevenue": "100000",
        }
        """)
public class OrderSummaryDTO {
    private Long totalOrders;
    private BigDecimal totalRevenue;
}
