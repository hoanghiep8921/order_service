package com.esoft.order_service.dto.request;

import com.esoft.order_service.anotation.ValueOfEnum;
import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.ServiceName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Represents a create request for an order",
        example = """
        {
          "customerId": 123456,
          "serviceName": "Service A",
          "category": "Category X",
          "quantity": 2,
          "description": "Description of the order",
          "totalPrice": 9999,
          "notes": "Additional notes about the order",
          "address": "HN"
        }
        """)
public class CreateOrderRequest {
    @Schema(description = "Information about the customer placing the order")
    @NotNull(message = "Customer ID must not be blank")
    private Long customerId;
    @Schema(description = "One of these values: PHOTO_EDITING, VIDEO_EDITING")
    @ValueOfEnum(enumClass = ServiceName.class)
    private String serviceName;
    @Schema(description = "One of these values: LUXURY, SUPER_LUXURY, SUPREME_LUXURY")
    @ValueOfEnum(enumClass = OrderCategory.class)
    private String category;
    @Schema(description = "Number of items to be served in this order")
    @Min(1)
    private Integer quantity;
    @NotNull(message = "Prices cannot be null")
    @PositiveOrZero(message = "Prices must be positive or zero")
    @Schema(description = "Total amount of the order")
    private BigDecimal totalPrice;
    private String address;
    private String description;
    private String notes;
}
