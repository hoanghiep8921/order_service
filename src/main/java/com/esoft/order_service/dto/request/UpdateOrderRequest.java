package com.esoft.order_service.dto.request;

import com.esoft.order_service.anotation.ValueOfEnum;
import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.ServiceName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Represents a update request for an order",
        example = """
        {
          "serviceName": "Service A",
          "category": "Category X",
          "quantity": 2,
          "description": "Description of the order",
          "notes": "Additional notes about the order",
          "address": "HN"
        }
        """)
public class UpdateOrderRequest {
    @Schema(description = "Number of items to be served in this order")
    private Integer quantity;
    @Schema(description = "One of these values: PHOTO_EDITING, VIDEO_EDITING")
    @ValueOfEnum(enumClass = ServiceName.class)
    private String serviceName;
    @Schema(description = "One of these values: LUXURY, SUPER_LUXURY, SUPREME_LUXURY")
    @ValueOfEnum(enumClass = OrderCategory.class)
    private String category;
    private String description;
    private String notes;
    private String address;
}
