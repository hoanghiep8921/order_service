package com.esoft.order_service.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Represents data response to client",
        example = """
        {
          "code": "00",
          "message": "Success",
          "data": {}
        }
        """)
public class BaseResponse<T> {
    @Schema(description = "Code represents the result of the request")
    private String code;
    @Schema(description = "Message content notifying the result of the request")
    private String message;
    @Schema(description = "Contains specific data for each request")
    private T data;
}
