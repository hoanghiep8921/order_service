package com.esoft.order_service.exceptions;

import java.io.Serial;
import java.io.Serializable;

public class ClientException extends RuntimeException implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    public static final String INVALID_DATA = "Invalid data";
    public ClientException(String message) {
        super(message);
    }
}
