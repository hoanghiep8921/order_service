package com.esoft.order_service.exceptions;


import com.esoft.order_service.constant.ServiceCode;
import com.esoft.order_service.dto.response.BaseResponse;
import com.esoft.order_service.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(ClientException.class)
    public ResponseEntity<BaseResponse<Object>> handlePokemonNotFoundException(ClientException ex, WebRequest request) {
        log.error("ClientException", ex);
        String errorMessage = ex.getMessage();
        BaseResponse<Object> errorDetails = new BaseResponse<>(ServiceCode.NOT_FOUND.getValue(), errorMessage, request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<BaseResponse<Object>> resourceNotFoundException(NotFoundException ex, WebRequest request) {
        log.error("NotFoundException", ex);
        String errorMessage = ex.getMessage();
        BaseResponse<Object> errorDetails = new BaseResponse<>(ServiceCode.NOT_FOUND.getValue(), errorMessage, request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse<Object>> globalExceptionHandler(Exception ex, WebRequest request) {
        log.error("GlobalExceptionHandler", ex);
        String errorMessage = ex.getMessage();
        return Utils.createErrorResponse(ServiceCode.ERROR.getValue(), errorMessage);
    }
}
