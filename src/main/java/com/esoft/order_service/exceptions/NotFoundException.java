package com.esoft.order_service.exceptions;

import java.io.Serial;
import java.io.Serializable;

public class NotFoundException extends RuntimeException implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    public static final String NOT_FOUND = "Data not found";
    public NotFoundException(String message) {
        super(message);
    }
}
