package com.esoft.order_service.models;

import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.OrderStatus;
import com.esoft.order_service.constant.ServiceName;
import lombok.*;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "ORDER_REQUEST",indexes = {
        @Index(name = "idx_orderCreatedAt", columnList = "orderCreatedAt"),
        @Index(name = "idx_customer_id",columnList = "customer_id"),
        @Index(name = "idx_serviceName_orderCreatedAt", columnList = "serviceName, orderCreatedAt")
})
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderEntity extends Auditable<String> implements Serializable {
    @Serial
    private static final long serialVersionUID = 12456789L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String orderReference;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OrderCategory orderCategory;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ServiceName serviceName;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OrderStatus status;
    @Column(nullable = false)
    private BigDecimal totalPrice;
    @Column(nullable = false)
    private Integer quantity;
    private String description;
    private String address;
    private String notes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;
}
