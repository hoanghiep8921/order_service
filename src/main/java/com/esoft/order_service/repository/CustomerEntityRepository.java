package com.esoft.order_service.repository;

import com.esoft.order_service.models.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerEntityRepository extends JpaRepository<CustomerEntity,Long> {
}
