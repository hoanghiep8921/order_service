package com.esoft.order_service.repository;

import com.esoft.order_service.dto.core.OrderSummaryDTO;
import com.esoft.order_service.dto.core.CustomerOrderSummaryDTO;
import com.esoft.order_service.dto.core.CustomerRevenueSummaryDTO;
import com.esoft.order_service.dto.core.ServiceTypeOrderSummaryDTO;
import com.esoft.order_service.models.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    /*
    * nativeQuery,temp table,partition time,... can be used to speed up queries
    * */
    Page<OrderEntity> findByCustomerId(Long id, Pageable pageable);

    @Query("SELECT new com.esoft.order_service.dto.core.CustomerOrderSummaryDTO(o.customer.id,o.customer.name, COUNT(o.customer.id)) FROM OrderEntity o GROUP BY o.customer.id")
    List<CustomerOrderSummaryDTO> countOrdersByCustomerId();

    @Query("SELECT new com.esoft.order_service.dto.core.CustomerRevenueSummaryDTO(o.customer.id,o.customer.name, SUM(o.totalPrice)) FROM OrderEntity o GROUP BY o.customer.id")
    List<CustomerRevenueSummaryDTO> calculateRevenueByCustomerId();

    @Query("SELECT new com.esoft.order_service.dto.core.OrderSummaryDTO(COUNT(o), SUM(o.totalPrice)) " +
            "FROM OrderEntity o " +
            "WHERE o.orderCreatedAt BETWEEN :startDate AND :endDate")
    OrderSummaryDTO getOrderSummary(@Param("startDate") LocalDateTime startDate,
                                    @Param("endDate") LocalDateTime endDate);

    @Query("SELECT new com.esoft.order_service.dto.core.ServiceTypeOrderSummaryDTO(o.serviceName, COUNT(o), SUM(o.totalPrice)) " +
            "FROM OrderEntity o " +
            "WHERE o.orderCreatedAt BETWEEN :startDate AND :endDate " +
            "GROUP BY o.serviceName")
    List<ServiceTypeOrderSummaryDTO> getOrderSummaryByServiceType(@Param("startDate") LocalDateTime startDate,
                                                                  @Param("endDate") LocalDateTime endDate);

}
