package com.esoft.order_service.service;

import com.esoft.order_service.dto.core.*;
import com.esoft.order_service.dto.request.CreateOrderRequest;
import com.esoft.order_service.dto.request.UpdateOrderRequest;
import com.esoft.order_service.exceptions.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderService {
    OrderDTO findById(Long orderId) throws NotFoundException;

    Page<OrderDTO> getUserOrders(Long userId, Pageable pageable);

    OrderDTO saveOrder(CreateOrderRequest order) throws NotFoundException;

    OrderDTO updateOrder(Long id, UpdateOrderRequest createConfigRequest) throws NotFoundException;

    void deleteById(Long orderId);

    List<CustomerOrderSummaryDTO> countOrdersByCustomerId();
    List<CustomerRevenueSummaryDTO> calculateRevenueByCustomerId();
    OrderSummaryDTO getOrderSummary(LocalDateTime startDate, LocalDateTime endDate);

    List<ServiceTypeOrderSummaryDTO> getOrderSummaryByServiceType(LocalDateTime startDate, LocalDateTime endDate);
}
