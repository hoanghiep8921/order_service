package com.esoft.order_service.service.impl;

import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.OrderStatus;
import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.dto.core.*;
import com.esoft.order_service.dto.request.CreateOrderRequest;
import com.esoft.order_service.dto.request.UpdateOrderRequest;
import com.esoft.order_service.exceptions.NotFoundException;
import com.esoft.order_service.models.CustomerEntity;
import com.esoft.order_service.models.OrderEntity;
import com.esoft.order_service.repository.CustomerEntityRepository;
import com.esoft.order_service.repository.OrderRepository;
import com.esoft.order_service.service.OrderService;
import com.esoft.order_service.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final CustomerEntityRepository customerEntityRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CustomerEntityRepository customerEntityRepository) {
        this.orderRepository = orderRepository;
        this.customerEntityRepository = customerEntityRepository;
    }

    @Override
    public OrderDTO findById(Long id) throws NotFoundException {
        OrderEntity orderEntity = this.orderRepository.findById(id).orElseThrow(() -> new NotFoundException(NotFoundException.NOT_FOUND));
        return Utils.getModelMapper().map(orderEntity, OrderDTO.class);
    }

    @Override
    public Page<OrderDTO> getUserOrders(Long customerId, Pageable pageable) {
        return orderRepository.findByCustomerId(customerId, pageable)
                .map(orderEntity -> Utils.getModelMapper().map(orderEntity, OrderDTO.class));
    }

    @Override
    public OrderDTO saveOrder(CreateOrderRequest createOrderRequest) throws NotFoundException  {
        CustomerEntity customer = customerEntityRepository.findById(createOrderRequest.getCustomerId()).orElseThrow(() -> new NotFoundException("Customer not found"));
        String serviceName = createOrderRequest.getServiceName();
        String category = createOrderRequest.getCategory();
        OrderEntity orderEntity = OrderEntity.builder()
                .customer(customer)
                .orderReference(Utils.generateOrderReference(serviceName))
                .serviceName(ServiceName.valueOf(serviceName))
                .orderCategory(OrderCategory.valueOf(category))
                .quantity(createOrderRequest.getQuantity())
                .totalPrice(createOrderRequest.getTotalPrice())
                .address(createOrderRequest.getAddress())
                .notes(createOrderRequest.getNotes())
                .description(createOrderRequest.getDescription())
                .status(OrderStatus.PENDING)
                .build();
        orderEntity = orderRepository.save(orderEntity);
        return Utils.getModelMapper().map(orderEntity, OrderDTO.class);
    }

    @Override
    public OrderDTO updateOrder(Long id, UpdateOrderRequest updateOrderRequest) throws NotFoundException {
        OrderEntity orderEntityExits = this.orderRepository.findById(id).orElseThrow(() -> new NotFoundException(NotFoundException.NOT_FOUND));
        String serviceName = updateOrderRequest.getServiceName();
        String category = updateOrderRequest.getCategory();
        if(StringUtils.hasText(serviceName)){
            orderEntityExits.setServiceName(ServiceName.valueOf(serviceName));
        }
        if(StringUtils.hasText(category)){
            orderEntityExits.setOrderCategory(OrderCategory.valueOf(category));
        }
        BeanUtils.copyProperties(updateOrderRequest, orderEntityExits, Utils.getNullPropertyNames(updateOrderRequest));
        orderRepository.save(orderEntityExits);
        return Utils.getModelMapper().map(orderEntityExits, OrderDTO.class);
    }

    @Override
    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public List<CustomerOrderSummaryDTO> countOrdersByCustomerId() {
        return orderRepository.countOrdersByCustomerId();
    }

    @Override
    public List<CustomerRevenueSummaryDTO> calculateRevenueByCustomerId() {
        return orderRepository.calculateRevenueByCustomerId();
    }

    @Override
    public OrderSummaryDTO getOrderSummary(LocalDateTime startDate, LocalDateTime endDate) {
        return orderRepository.getOrderSummary(startDate, endDate);
    }

    @Override
    public List<ServiceTypeOrderSummaryDTO> getOrderSummaryByServiceType(LocalDateTime startDate, LocalDateTime endDate) {
        return orderRepository.getOrderSummaryByServiceType(startDate, endDate);
    }
}
