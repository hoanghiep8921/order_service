package com.esoft.order_service.util;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import static java.lang.String.format;
import static java.lang.System.out;

// example only shows the logic. Adding any Logging Libraries and Tools is straight forward by replacing console output lines with Logging frameworks
interface Log {
    private <R> R execute(String message, Callable<R> fn) throws Exception {
        var stack = StackWalker.getInstance().walk(frames -> frames.skip(2).findFirst().orElse(null));
        var executedBy = stack != null
                ? format("%s.%s", stack.getClassName(), stack.getMethodName())
                : "NO_METHOD";

        try {
            out.printf("[%s] start %s%n", executedBy, message);
            var output = fn.call();
            out.printf("[%s] finished %s%n", executedBy, message);

            return output;
        } catch (Exception e) {
            out.printf("[%s] failed %s%n", executedBy, message);
            throw e;
        }
    }

    default void log(@NotBlank String message, @NotNull Runnable fn) throws Exception {
        execute(message, () -> {
            fn.run();
            return null;
        });
    }

    default <R> @NotNull R log(@NotBlank String message, @NotNull Supplier<R> fn) throws Exception {
        return execute(message, fn::get);
    }
}