package com.esoft.order_service.util;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Component
public class SQLAnalyzer {
    private final EntityManagerFactory entityManagerFactory;

    public SQLAnalyzer(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void analyzeQuery(String sql) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            List<Object> result = entityManager.createNativeQuery(sql).getResultList();
        } finally {
            entityManager.close();
        }
    }
}
