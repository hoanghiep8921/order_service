package com.esoft.order_service.util;

import com.esoft.order_service.constant.MessageResponse;
import com.esoft.order_service.constant.ServiceCode;
import com.esoft.order_service.dto.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.beans.PropertyDescriptor;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Slf4j
public class Utils {
    public static final String ORD = "ORD";
    private static class MapHelper {
        private static final ModelMapper modelMapper = new ModelMapper();
    }
    public static ModelMapper getModelMapper() {
        return MapHelper.modelMapper;
    }

    public static String generateOrderReference(String serviceName) {
        StringBuilder orderReference = new StringBuilder();
        String timestampPart = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String servicePart = serviceName.substring(0, Math.min(serviceName.length(), 3)).toUpperCase();
        String uniquePart = UUID.randomUUID().toString().replace("-", "").substring(0, 6);
        orderReference.append(ORD);
        if (!timestampPart.isEmpty()) {
            orderReference.append(timestampPart).append("-");
        }
        if (!servicePart.isEmpty()) {
            orderReference.append(servicePart).append("-");
        }
        orderReference.append(uniquePart);
        return orderReference.toString();
    }

    public static <T> ResponseEntity<BaseResponse<T>> createSuccessResponse(T data) {
        BaseResponse<T> baseResponse = new BaseResponse<>(ServiceCode.SUCCESS.getValue(), MessageResponse.SUCCESS.getValue(""), data);
        return ResponseEntity.status(HttpStatus.OK).body(baseResponse);
    }
    public static <T> ResponseEntity<BaseResponse<T>> createSuccessResponse(String language, boolean isCreated) {
        return createSuccessResponse(null,language, isCreated);
    }

    public static <T> ResponseEntity<BaseResponse<T>> createSuccessResponse(T data,String language, boolean isCreated) {
        HttpStatus status = isCreated ? HttpStatus.CREATED : HttpStatus.OK;
        BaseResponse<T> baseResponse = new BaseResponse<>(ServiceCode.SUCCESS.getValue(), MessageResponse.SUCCESS.getValue(language), data);
        return ResponseEntity.status(status).body(baseResponse);
    }

    public static <T> ResponseEntity<BaseResponse<T>> createErrorResponse(String errorCode, String errorMessage) {
        return createErrorResponse(errorCode, errorMessage, null);
    }

    public static <T> ResponseEntity<BaseResponse<T>> createErrorResponse(String errorCode, String errorMessage, T data) {
        BaseResponse<T> baseResponse = new BaseResponse<>(errorCode, errorMessage, data);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponse);
    }

    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd : src.getPropertyDescriptors()) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
