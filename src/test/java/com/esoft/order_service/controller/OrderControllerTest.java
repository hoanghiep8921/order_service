package com.esoft.order_service.controller;

import com.esoft.order_service.constant.CustomerType;
import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.OrderStatus;
import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.controllers.OrderController;
import com.esoft.order_service.dto.core.OrderDTO;
import com.esoft.order_service.dto.request.CreateOrderRequest;
import com.esoft.order_service.dto.request.UpdateOrderRequest;
import com.esoft.order_service.models.CustomerEntity;
import com.esoft.order_service.models.OrderEntity;
import com.esoft.order_service.service.impl.OrderServiceImpl;
import com.esoft.order_service.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(controllers = OrderController.class)
@AutoConfigureMockMvc(addFilters = true)
@ExtendWith(MockitoExtension.class)
class OrderControllerTest {
    private final ModelMapper modelMapper = new ModelMapper();
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private OrderServiceImpl orderService;
    private OrderEntity order;
    private OrderDTO orderDTO;
    private CreateOrderRequest createOrderRequest;

    private String authHeader;

    @Value("${spring.security.user.name}")
    private String username;

    @Value("${spring.security.user.password}")
    private String password;

    @BeforeEach
    void init() {
        String noteFake = "note fake";
        String descriptionFake = "description fake";
        String addressFake = "address fake";
        int quantityFake = 1;
        BigDecimal priceFake = new BigDecimal(1000);
        CustomerEntity customer = CustomerEntity.builder().id(1L).customerType(CustomerType.CUSTOMER).name("Customer").build();

        order = OrderEntity.builder().id(1L).status(OrderStatus.PENDING).customer(customer).orderCategory(OrderCategory.LUXURY).serviceName(ServiceName.VIDEO_EDITING).notes(noteFake).description(descriptionFake).address(addressFake).quantity(quantityFake).totalPrice(priceFake).orderReference(Utils.generateOrderReference(ServiceName.VIDEO_EDITING.toString())).build();
        orderDTO = modelMapper.map(order, OrderDTO.class);
        createOrderRequest = CreateOrderRequest.builder().customerId(customer.getId()).category(OrderCategory.LUXURY.toString()).serviceName(ServiceName.VIDEO_EDITING.toString()).notes(noteFake).description(descriptionFake).address(addressFake).quantity(quantityFake).totalPrice(priceFake).build();
        authHeader = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());

    }

    @Test
    void unauthorized_returnError() throws Exception {
        // Given
        given(orderService.saveOrder(ArgumentMatchers.any())).willAnswer((invocation -> orderDTO));
        String requestBody = objectMapper.writeValueAsString(createOrderRequest);
        ResultActions response = mockMvc.perform(post("/api/v1/orders/create").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        response.andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    void createOrder_returnCreated() throws Exception {
        // Given
        given(orderService.saveOrder(ArgumentMatchers.any())).willAnswer((invocation -> orderDTO));
        String requestBody = objectMapper.writeValueAsString(createOrderRequest);
        ResultActions response = mockMvc.perform(post("/api/v1/orders/create").header(HttpHeaders.AUTHORIZATION, authHeader).contentType(MediaType.APPLICATION_JSON).content(requestBody));

        response.andExpect(MockMvcResultMatchers.status().isCreated()).andExpect(MockMvcResultMatchers.jsonPath("$.data.quantity", CoreMatchers.is(orderDTO.getQuantity()))).andExpect(MockMvcResultMatchers.jsonPath("$.data.serviceName", CoreMatchers.is(orderDTO.getServiceName())));
    }

    @Test
    void getAllOrderByCustomer_returnResponseDTO() throws Exception {

        List<OrderDTO> orderDTOS = Collections.singletonList(orderDTO);
        Page<OrderDTO> pageDTO = new PageImpl<>(orderDTOS, PageRequest.of(0, 10), 1);

        when(orderService.getUserOrders(1L, PageRequest.of(0, 10))).thenReturn(pageDTO);

        ResultActions response = mockMvc.perform(get("/api/v1/orders").contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, authHeader).param("customerId", "1").param("page", "0").param("size", "10"));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.content.size()", CoreMatchers.is(pageDTO.getContent().size())));

    }


    @Test
    void getDetail_returnOrderDTO() throws Exception {
        when(orderService.findById(order.getId())).thenReturn(orderDTO);
        ResultActions response = mockMvc.perform(get("/api/v1/orders/" + order.getId()).header(HttpHeaders.AUTHORIZATION, authHeader));
        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.quantity", CoreMatchers.is(orderDTO.getQuantity()))).andExpect(MockMvcResultMatchers.jsonPath("$.data.serviceName", CoreMatchers.is(orderDTO.getServiceName())));
    }

    @Test
    void updateOrder_returnOrderDTO() throws Exception {
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setAddress("edited address");
        orderDTO.setAddress(updateOrderRequest.getAddress());
        when(orderService.updateOrder(orderDTO.getId(), updateOrderRequest)).thenReturn(orderDTO);

        ResultActions response = mockMvc.perform(put("/api/v1/orders/1/update").contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, authHeader).content(objectMapper.writeValueAsString(updateOrderRequest)));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.address", CoreMatchers.is(updateOrderRequest.getAddress())));
    }

    @Test
    void deleteOrder_returnOK() throws Exception {
        doNothing().when(orderService).deleteById(order.getId());

        ResultActions response = mockMvc.perform(delete("/api/v1/orders/1/delete").header(HttpHeaders.AUTHORIZATION, authHeader).contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk());
    }
}
