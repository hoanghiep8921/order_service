package com.esoft.order_service.controller;

import com.esoft.order_service.controllers.ReportController;
import com.esoft.order_service.dto.core.CustomerOrderSummaryDTO;
import com.esoft.order_service.dto.core.CustomerRevenueSummaryDTO;
import com.esoft.order_service.dto.core.OrderSummaryDTO;
import com.esoft.order_service.service.impl.OrderServiceImpl;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@WebMvcTest(controllers = ReportController.class)
@AutoConfigureMockMvc(addFilters = true)
@ExtendWith(MockitoExtension.class)
class ReportControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private OrderServiceImpl orderService;
    private String authHeader;

    @Value("${spring.security.user.name}")
    private String username;

    @Value("${spring.security.user.password}")
    private String password;

    @BeforeEach
    void init() {
        authHeader = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }

    @Test
    void getReportOrderSummary_returnOrderSummaryDTO() throws Exception {
        OrderSummaryDTO orderSummaryDTO = OrderSummaryDTO.builder().totalOrders(1000L).totalRevenue(new BigDecimal(100000)).build();
        when(orderService.getOrderSummary(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(orderSummaryDTO);

        ResultActions response = mockMvc.perform(get("/api/v1/reports/order-summaries").header(HttpHeaders.AUTHORIZATION, authHeader).param("startDate", "2024-01-01T00:00:00").param("endDate", "2024-04-01T00:00:00"));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.totalOrders", CoreMatchers.is(1000)));

    }

    @Test
    void getReportServiceTypeOrderSummaries_returnEmpty() throws Exception {
        when(orderService.getOrderSummaryByServiceType(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.emptyList());

        ResultActions response = mockMvc.perform(get("/api/v1/reports/service-type-order-summaries").header(HttpHeaders.AUTHORIZATION, authHeader).param("startDate", "2024-01-01T00:00:00").param("endDate", "2024-04-01T00:00:00"));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.size()", CoreMatchers.is(0)));

    }

    @Test
    void getReportCustomerRevenueSummaries_returnListCustomerRevenueSummaryDTO() throws Exception {
        CustomerRevenueSummaryDTO customerRevenueSummaryDTO = CustomerRevenueSummaryDTO.builder().totalRevenue(new BigDecimal(100000)).customerId(1L).name("hiepdh").build();
        when(orderService.calculateRevenueByCustomerId()).thenReturn(Collections.singletonList(customerRevenueSummaryDTO));

        ResultActions response = mockMvc.perform(get("/api/v1/reports/customer-revenue-summaries").header(HttpHeaders.AUTHORIZATION, authHeader));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.size()", CoreMatchers.is(1))).andExpect(MockMvcResultMatchers.jsonPath("$.data[0].totalRevenue", CoreMatchers.is(100000))).andExpect(MockMvcResultMatchers.jsonPath("$.data[0].customerId", CoreMatchers.is(1)));

    }

    @Test
    void getReportCustomerRevenueSummaries_returnEmpty() throws Exception {
        when(orderService.calculateRevenueByCustomerId()).thenReturn(Collections.emptyList());

        ResultActions response = mockMvc.perform(get("/api/v1/reports/customer-revenue-summaries").header(HttpHeaders.AUTHORIZATION, authHeader));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.size()", CoreMatchers.is(0)));


    }

    @Test
    void getReportCustomerOrderSummaries_returnListServiceTypeOrderSummaryDTO() throws Exception {
        CustomerOrderSummaryDTO serviceTypeOrderSummaryDTO = CustomerOrderSummaryDTO.builder().orderCount(100L).name("hiepdh").customerId(1L).build();
        when(orderService.countOrdersByCustomerId()).thenReturn(Collections.singletonList(serviceTypeOrderSummaryDTO));

        ResultActions response = mockMvc.perform(get("/api/v1/reports/customer-order-summaries").header(HttpHeaders.AUTHORIZATION, authHeader));

        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.size()", CoreMatchers.is(1))).andExpect(MockMvcResultMatchers.jsonPath("$.data[0].orderCount", CoreMatchers.is(100))).andExpect(MockMvcResultMatchers.jsonPath("$.data[0].customerId", CoreMatchers.is(1)));

    }

    @Test
    void getReportCustomerOrderSummaries_returnEmpty() throws Exception {
        when(orderService.countOrdersByCustomerId()).thenReturn(Collections.emptyList());
        ResultActions response = mockMvc.perform(get("/api/v1/reports/customer-order-summaries").header(HttpHeaders.AUTHORIZATION, authHeader));
        response.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.data.size()", CoreMatchers.is(0)));
    }
}
