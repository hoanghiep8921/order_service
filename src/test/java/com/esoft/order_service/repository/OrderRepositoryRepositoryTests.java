package com.esoft.order_service.repository;

import com.esoft.order_service.constant.CustomerType;
import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.OrderStatus;
import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.dto.core.OrderSummaryDTO;
import com.esoft.order_service.dto.core.CustomerOrderSummaryDTO;
import com.esoft.order_service.dto.core.CustomerRevenueSummaryDTO;
import com.esoft.order_service.models.CustomerEntity;
import com.esoft.order_service.models.OrderEntity;
import com.esoft.order_service.util.Utils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class OrderRepositoryRepositoryTests {

    private final OrderRepository orderRepository;
    private final CustomerEntityRepository customerEntityRepository;
    private CustomerEntity customer;

    @Autowired
    public OrderRepositoryRepositoryTests(OrderRepository orderRepository, CustomerEntityRepository customerEntityRepository) {
        this.orderRepository = orderRepository;
        this.customerEntityRepository = customerEntityRepository;
    }

    @BeforeEach
    void init() {
        customer = generateCustomer();
        customer = customerEntityRepository.save(customer);
    }

    private CustomerEntity generateCustomer() {
        return CustomerEntity.builder().id(1L).customerType(CustomerType.CUSTOMER).name("Customer").build();
    }

    private OrderEntity generateOrder() {
        String noteFake = "note fake";
        String descriptionFake = "description fake";
        String addressFake = "address fake";
        int quantityFake = 1;
        BigDecimal priceFake = new BigDecimal(1000);
        return OrderEntity.builder().status(OrderStatus.PENDING).customer(customer).orderCategory(OrderCategory.LUXURY).serviceName(ServiceName.VIDEO_EDITING).notes(noteFake).description(descriptionFake).address(addressFake).quantity(quantityFake).totalPrice(priceFake).orderReference(Utils.generateOrderReference(ServiceName.VIDEO_EDITING.toString())).build();
    }

    @Test
    void saveAll_returnsOrderEntity() {
        OrderEntity order = generateOrder();
        OrderEntity orderEntity = orderRepository.save(order);
        assertNotNull(orderEntity);
        assertTrue(orderEntity.getId() > 0);
    }

    @Test
    void getAll_returnsListOrder() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        List<OrderEntity> orderEntities = orderRepository.findAll();
        assertNotNull(orderEntities);
        assertEquals(2, orderEntities.size());
    }

    @Test
    void findById_returnOrderEntity() {
        OrderEntity order = generateOrder();
        order = orderRepository.save(order);
        OrderEntity orderEntity = orderRepository.findById(order.getId()).get();
        assertNotNull(orderEntity);
    }

    @Test
    void updateOrder_returnOrderEntity() {
        String strEditedAddress = "address edited";
        OrderEntity order = generateOrder();
        orderRepository.save(order);
        OrderEntity orderEntity = orderRepository.findById(order.getId()).get();
        orderEntity.setAddress(strEditedAddress);
        orderEntity.setServiceName(ServiceName.PHOTO_EDITING);
        OrderEntity updatedOrder = orderRepository.save(orderEntity);
        assertNotNull(updatedOrder.getAddress());
        assertNotNull(updatedOrder.getServiceName());
        assertEquals(strEditedAddress, updatedOrder.getAddress());
    }

    @Test
    void deleteOrder_returnIsEmpty() {
        OrderEntity order = generateOrder();
        order = orderRepository.save(order);
        orderRepository.deleteById(order.getId());
        Optional<OrderEntity> orderEntity = orderRepository.findById(order.getId());
        assertTrue(orderEntity.isEmpty());
    }

    @Test
    void findOrderByCustomer_returnListOrder() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        Pageable pageable = PageRequest.of(0, 10);
        Page<OrderEntity> orders = orderRepository.findByCustomerId(customer.getId(), pageable);
        assertNotNull(orders);
        assertEquals(2, orders.getContent().size());
    }

    @Test
    void findOrderByCustomerNotfound_returnEmpty() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        Pageable pageable = PageRequest.of(0, 10);
        Page<OrderEntity> orders = orderRepository.findByCustomerId(100L, pageable);
        assertEquals(0, orders.getContent().size());
    }

    @Test
    void countOrdersByCustomerId_ReturnsOrderSummaryDTOList() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        List<CustomerOrderSummaryDTO> customerOrderSummaryDTOList = orderRepository.countOrdersByCustomerId();
        assertNotNull(customerOrderSummaryDTOList);
        assertEquals(1, customerOrderSummaryDTOList.size()); //equal total customer
    }


    @Test
    void calculateRevenueByCustomerId_ReturnsRevenueSummaryDTOList() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        List<CustomerRevenueSummaryDTO> customerRevenueSummaryDTOList = orderRepository.calculateRevenueByCustomerId();
        assertNotNull(customerRevenueSummaryDTOList);
        // Kiểm tra dữ liệu trả về ở đây nếu cần
    }

    @Test
    void getOrdersAndRevenueInTimeRange_ReturnsOrderReportDTO() {
        OrderEntity order1 = generateOrder();
        OrderEntity order2 = generateOrder();
        orderRepository.save(order1);
        orderRepository.save(order2);
        LocalDateTime startDate = LocalDateTime.now().minusDays(7);
        LocalDateTime endDate = LocalDateTime.now();
        OrderSummaryDTO orderSummaryDTO = orderRepository.getOrderSummary(startDate, endDate);
        assertNotNull(orderSummaryDTO);
    }
}
