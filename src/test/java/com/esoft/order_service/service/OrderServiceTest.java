package com.esoft.order_service.service;

import com.esoft.order_service.constant.CustomerType;
import com.esoft.order_service.constant.OrderCategory;
import com.esoft.order_service.constant.OrderStatus;
import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.dto.core.OrderDTO;
import com.esoft.order_service.dto.request.CreateOrderRequest;
import com.esoft.order_service.dto.request.UpdateOrderRequest;
import com.esoft.order_service.exceptions.NotFoundException;
import com.esoft.order_service.models.CustomerEntity;
import com.esoft.order_service.models.OrderEntity;
import com.esoft.order_service.repository.CustomerEntityRepository;
import com.esoft.order_service.repository.OrderRepository;
import com.esoft.order_service.service.impl.OrderServiceImpl;
import com.esoft.order_service.util.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @Mock
    private CustomerEntityRepository customerEntityRepository;

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    private CustomerEntity customer;
    private OrderEntity order;
    private OrderDTO orderDTO;
    private CreateOrderRequest createOrderRequest;

    @BeforeEach
    void init() {
        String noteFake = "note fake";
        String descriptionFake = "description fake";
        String addressFake = "address fake";
        int quantityFake = 1;
        BigDecimal priceFake = new BigDecimal(1000);
        customer = CustomerEntity.builder().id(1L).customerType(CustomerType.CUSTOMER).name("Customer").build();

        order = OrderEntity.builder().id(1L).status(OrderStatus.PENDING).customer(customer).orderCategory(OrderCategory.LUXURY).serviceName(ServiceName.VIDEO_EDITING).notes(noteFake).description(descriptionFake).address(addressFake).quantity(quantityFake).totalPrice(priceFake).orderReference(Utils.generateOrderReference(ServiceName.VIDEO_EDITING.toString())).build();
        orderDTO = Utils.getModelMapper().map(order, OrderDTO.class);
        createOrderRequest = CreateOrderRequest.builder().customerId(customer.getId()).category(OrderCategory.LUXURY.toString()).serviceName(ServiceName.VIDEO_EDITING.toString()).notes(noteFake).description(descriptionFake).address(addressFake).quantity(quantityFake).totalPrice(priceFake).build();
    }

    @Test
    void createOrder_returnsOrderDTO() throws NotFoundException {
        when(customerEntityRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
        when(orderRepository.save(Mockito.any(OrderEntity.class))).thenReturn(order);
        OrderDTO saveOrder = orderService.saveOrder(createOrderRequest);
        assertNotNull(saveOrder);
        assertEquals(Long.valueOf(1L), orderDTO.getId());
    }

    @Test
    void createOrder_customerNotFound() {
        when(customerEntityRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> orderService.saveOrder(createOrderRequest));
    }

    @Test
    void getOrderByCustomerId_returnPageOrderDTO() {
        List<OrderEntity> orderEntities = Collections.singletonList(order);
        Pageable pageable = PageRequest.of(0, 10);
        Page<OrderEntity> page = new PageImpl<>(orderEntities, pageable, orderEntities.size());
        when(orderRepository.findByCustomerId(1L, PageRequest.of(0, 10))).thenReturn(page);
        Page<OrderDTO> orderDTOPage = orderService.getUserOrders(1L, pageable);
        assertNotNull(orderDTOPage);
        assertEquals(orderEntities.size(), orderDTOPage.getContent().size());
    }

    @Test
    void getOrderByCustomerId_returnPageOrderDTOEmpty() {
        List<OrderEntity> orderEntities = Collections.emptyList();
        Pageable pageable = PageRequest.of(0, 10);
        Page<OrderEntity> page = new PageImpl<>(orderEntities, pageable, 0);
        when(orderRepository.findByCustomerId(1L, PageRequest.of(0, 10))).thenReturn(page);
        Page<OrderDTO> orderDTOPage = orderService.getUserOrders(1L, pageable);
        assertNotNull(orderDTOPage);
        assertEquals(0, orderDTOPage.getContent().size());
    }

    @Test
    void getOrderById_returnOrderDTO() throws NotFoundException {
        when(orderRepository.findById(1L)).thenReturn(Optional.of(order));
        OrderDTO orderDTO = orderService.findById(1L);
        assertNotNull(orderDTO);
    }

    @Test
    void getOrderById_recordNotFound() {
        when(orderRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> orderService.findById(1L));
    }

    @Test
    void deleteOrderById_returnOrderDTO() {
        orderService.deleteById(1L);
        verify(orderRepository).deleteById(1L);
    }

    @Test
    void deleteOrderById_recordNotFound() {
        doThrow(EmptyResultDataAccessException.class).when(orderRepository).deleteById(1L);
        assertThrows(EmptyResultDataAccessException.class, () -> orderService.deleteById(1L));
    }

    @Test
    void updateOrderById_returnOrderDTO() throws NotFoundException {
        when(orderRepository.findById(1L)).thenReturn(Optional.of(order));
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setServiceName(ServiceName.PHOTO_EDITING.toString());
        updateOrderRequest.setCategory(OrderCategory.SUPREME_LUXURY.toString());
        updateOrderRequest.setAddress("address edited");

        OrderDTO updatedOrderDTO = orderService.updateOrder(1L, updateOrderRequest);
        verify(orderRepository).findById(1L);

        assertEquals(ServiceName.PHOTO_EDITING.toString(), updatedOrderDTO.getServiceName());
        assertEquals(OrderCategory.SUPREME_LUXURY.toString(), updatedOrderDTO.getOrderCategory());

        verify(orderRepository).save(order);

        assertNotNull(updatedOrderDTO);
        assertEquals(1L, updatedOrderDTO.getId());
        assertEquals(updateOrderRequest.getServiceName(), updatedOrderDTO.getServiceName());
        assertEquals(updateOrderRequest.getCategory(), updatedOrderDTO.getOrderCategory());
        assertEquals(updateOrderRequest.getAddress(), updatedOrderDTO.getAddress());
    }

    @Test
    void updateOrderById_OrderNotFound() {
        when(orderRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> orderService.updateOrder(1L, new UpdateOrderRequest()));
        verify(orderRepository).findById(1L);
        verify(orderRepository, never()).save(any());
    }
}
