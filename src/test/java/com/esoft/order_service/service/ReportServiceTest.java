package com.esoft.order_service.service;

import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.dto.core.*;
import com.esoft.order_service.repository.OrderRepository;
import com.esoft.order_service.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportServiceTest {
    @Mock
    OrderRepository orderRepository;
    @InjectMocks
    OrderServiceImpl orderService;

    @Test
    void countOrdersByCustomerId() {
        List<CustomerOrderSummaryDTO> expected = Arrays.asList(
                new CustomerOrderSummaryDTO(1L,"hiepdh", 5L),
                new CustomerOrderSummaryDTO(2L,"admin", 3L)
        );
        when(orderRepository.countOrdersByCustomerId()).thenReturn(expected);

        List<CustomerOrderSummaryDTO> result = orderService.countOrdersByCustomerId();
        assertEquals(expected, result);
    }

    @Test
    void calculateRevenueByCustomerId() {
        List<CustomerRevenueSummaryDTO> expected = Arrays.asList(
                new CustomerRevenueSummaryDTO(1L,"hiepdh", new BigDecimal(10000)),
                new CustomerRevenueSummaryDTO(2L,"admin", new BigDecimal(5000))
        );
        when(orderRepository.calculateRevenueByCustomerId()).thenReturn(expected);

        List<CustomerRevenueSummaryDTO> result = orderService.calculateRevenueByCustomerId();
        assertEquals(expected, result);
    }

    @Test
    void getOrderSummary() {
        LocalDateTime startDate = LocalDateTime.of(2024, 5, 1, 0, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2024, 5, 31, 23, 59, 59);
        OrderSummaryDTO expected = new OrderSummaryDTO(100L, new BigDecimal(100000));
        when(orderRepository.getOrderSummary(startDate, endDate)).thenReturn(expected);

        OrderSummaryDTO result = orderService.getOrderSummary(startDate, endDate);
        assertEquals(expected, result);
    }

    @Test
    void getOrderSummaryByServiceType() {
        LocalDateTime startDate = LocalDateTime.of(2023, 5, 1, 0, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2023, 5, 31, 23, 59, 59);
        List<ServiceTypeOrderSummaryDTO> expected = Arrays.asList(
                new ServiceTypeOrderSummaryDTO(ServiceName.PHOTO_EDITING, 50L, new BigDecimal(50000)),
                new ServiceTypeOrderSummaryDTO(ServiceName.VIDEO_EDITING, 25L, new BigDecimal(25000))
        );
        when(orderRepository.getOrderSummaryByServiceType(startDate, endDate)).thenReturn(expected);

        List<ServiceTypeOrderSummaryDTO> result = orderService.getOrderSummaryByServiceType(startDate, endDate);
        assertEquals(expected, result);
    }
}
