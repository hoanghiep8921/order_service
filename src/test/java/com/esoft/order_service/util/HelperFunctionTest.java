package com.esoft.order_service.util;

import com.esoft.order_service.constant.CustomerType;
import com.esoft.order_service.constant.ServiceName;
import com.esoft.order_service.dto.core.CustomerDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@Slf4j
class HelperFunctionTest {

    @Test
    void testGenerateOrderReferenceWithoutServiceName() {
        String result = Utils.generateOrderReference("");
        String[] parts = result.split("-");
        assertEquals(24,result.length());
        assertEquals(2,parts.length);
    }

    @Test
    void testGenerateOrderReference() {
        String result = Utils.generateOrderReference(ServiceName.VIDEO_EDITING.toString());
        String[] parts = result.split("-");
        assertEquals(28,result.length());
        assertEquals(3,parts.length);
    }


    @Test
    void testNullPropertyNames_NewObjectWithAllFieldsFilled() {
        CustomerDTO customerDTO = CustomerDTO.builder().
                id(1L).
                customerType(CustomerType.CUSTOMER.toString()).
                name("Đoàn Hoàng Hiệp").build();
        String[] result = Utils.getNullPropertyNames(customerDTO);
        assertArrayEquals(new String[]{}, result);
    }

    @Test
    void testNullPropertyNames_NewObjectWithSomeFieldsFilled() {
        CustomerDTO customerDTO = CustomerDTO.builder().id(1L).name("").build();
        String[] result = Utils.getNullPropertyNames(customerDTO);
        assertArrayEquals(new String[]{"customerType"}, result);
    }

    @Test
    void testNullPropertyNames_NullObject() {
        CustomerDTO customerDTO = null;
        assertThrows(IllegalArgumentException.class, () -> Utils.getNullPropertyNames(customerDTO));
    }
}
